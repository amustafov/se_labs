import json
from collections import Counter
from bokeh.plotting import figure, show, output_file

if __name__ == "__main__":
    with open("birthdays.json", "r") as f:
        birthdays = json.load(f)

    num_to_string = {
        1: "January",
        2: "February",
        3: "March",
        4: "April",
        5: "May",
        6: "June",
        7: "July",
        8: "August",
        9: "September",
        10: "October",
        11: "November",
        12: "December"
    }

    a = []

    for i in birthdays.values():
        a.append(num_to_string[int(i[3:-5])])

    c = Counter(a)
    print(c)

    # Bokeh
    output_file("plot.html")

    x_categories = list(num_to_string.values())
    x = list(c.keys())
    y = list(c.values())

    p = figure(x_range=x_categories)
    p.vbar(x=x, top=y, width=0.5)

    show(p)